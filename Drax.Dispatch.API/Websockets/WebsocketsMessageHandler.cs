﻿using System.Collections.Generic;
using System.Linq;
using Drax.Dispatch.API.CommandHandlers;
using Microsoft.Web.WebSockets;
using Newtonsoft.Json;

namespace Drax.Dispatch.API.Websockets
{
    public class WebsocketsMessageHandler : WebSocketHandler
    {
        private static WebSocketCollection _wsClients = new WebSocketCollection();
        public  IList<CommandHandler> Handlers { get; } 

        public WebsocketsMessageHandler(IEnumerable<CommandHandler> handlers)
        {
           Handlers = new List<CommandHandler>(handlers);
        }
        public override void OnOpen()

        {
            _wsClients.Add(this);
            base.OnOpen();

        }

        public override void OnClose()

        {
            _wsClients.Remove(this);
            base.OnClose();

        }

        public void BroadcastMessageToAll(dynamic message)
        {
            _wsClients.Broadcast(JsonConvert.SerializeObject(message));
        }

        public override void OnMessage(string message)
        {
            if (message.Contains(@"""Type"""))
            {
                var command = JsonConvert.DeserializeObject<dynamic>(message);
                var response = HandleCommand(command);
                _wsClients.Broadcast(JsonConvert.SerializeObject(response));
            }
            else
            {
                _wsClients.Broadcast(" Message format error");
            }
           
           
            
        }
       

        private dynamic HandleCommand(dynamic command)
        {
            string commandText = command.Type;
            var handler = Handlers.FirstOrDefault(h => h.CanHandle(commandText) );
            return handler.Handle(command);
        }
    }
}