﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.WebSockets;
using Drax.Dispatch.API.CommandHandlers;
using Drax.Dispatch.API.Websockets;

namespace Drax.Dispatch.API.Controllers
{
    [RoutePrefix("")]
    public class HomeController : ApiController
    {
        [Route("")]
        public Task<HttpResponseMessage> Get()
        {
            if (HttpContext.Current.IsWebSocketRequest || HttpContext.Current.IsWebSocketRequestUpgrading)
            {
                HttpContext.Current.AcceptWebSocketRequest(HandleWebsocketRequest);
                return Task.FromResult(Request.CreateResponse(HttpStatusCode.SwitchingProtocols));
            }
            return Task.FromResult(Request.CreateResponse(HttpStatusCode.BadRequest));
        }

        private Task HandleWebsocketRequest(AspNetWebSocketContext webSocketContext)
        {
            var websocketsHandler = new WebsocketsMessageHandler(CommandHandlerFactory.GetCommandHandlers());
            return websocketsHandler.ProcessWebSocketRequestAsync(webSocketContext);
        }
    }
}
