﻿using System.Dynamic;

namespace Drax.Dispatch.API.CommandHandlers
{
    public abstract class CommandHandler
    {
        protected CommandHandler()
        {
            Response = new ExpandoObject();
           
        }
        protected dynamic Response { get; }
        public string Command { get; protected set; }
        public abstract void AddResponse();

        public  virtual bool CanHandle(string command)
        {
            return command == Command;
        }

        
        public dynamic Handle(dynamic command)
        {
            AddResponse();
            Response.Command = command;
            Response.ExecutedHandler = GetType().Name;
            return Response;

        }

    }
}