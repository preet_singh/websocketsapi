﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Windows.Input;

namespace Drax.Dispatch.API.CommandHandlers
{
    public class CommandHandlerFactory
    {
        public static IEnumerable<CommandHandler> GetCommandHandlers()
        {
            var handlers = new List<CommandHandler>
            {
                new TestCommandHandler(),
                new SelectedDateChangedHandler(),
                new DefaultHandler() // this should always be the last handler. its a catch all
            };
            
            return handlers;
        }
    }
}