﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Drax.Dispatch.API.CommandHandlers
{
    public class SelectedDateChangedHandler: CommandHandler
    {
        public SelectedDateChangedHandler()
        {
            Command = "SELECTED_DATE_CHANGED";
        }

        public override void AddResponse()
        {
            
        }
    }
}