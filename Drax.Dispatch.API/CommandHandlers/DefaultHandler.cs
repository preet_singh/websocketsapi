﻿namespace Drax.Dispatch.API.CommandHandlers
{
    public class DefaultHandler : CommandHandler
    {
        public override bool CanHandle(string command)
        {
            return true;
        }

        public override void AddResponse()
        {
            Response.Message = "No handler was found";
        }
    }
}